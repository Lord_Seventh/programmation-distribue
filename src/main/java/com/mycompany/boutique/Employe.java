/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;
import java.time.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author TIASS
 */
public class Employe extends Personne {
    private String cnss;
    private LocalDate dateEmbauche;
    private ArrayList<Achat> achats;
   
        public Employe(String cnss, LocalDate dateEmbauche, long id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    public String getCnss() {
        return cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employe)) return false;
        if (!super.equals(o)) return false;
        Employe employe = (Employe) o;
        return getCnss().equals(employe.getCnss()) && getDateEmbauche().equals(employe.getDateEmbauche()) && achats.equals(employe.achats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCnss(), getDateEmbauche(), achats);
    }

    @Override
    public String toString() {
        return "Employe{" +
                "cnss='" + cnss + '\'' +
                ", dateEmbauche=" + dateEmbauche +
                ", achats=" + achats +
                '}';
    }
}
