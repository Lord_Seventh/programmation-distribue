/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;
import java.time.*;
import java.util.Objects;

/**
 *
 * @author TIASS
 */
public class Client extends Personne {
    private String carteVisa;

    public Client(String carteVisa, long id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        if (!super.equals(o)) return false;
        Client client = (Client) o;
        return Objects.equals(getCarteVisa(), client.getCarteVisa());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCarteVisa());
    }

    @Override
    public String toString() {
        return "Client{" +
                "carteVisa='" + carteVisa + '\'' +
                '}';
    }
}

